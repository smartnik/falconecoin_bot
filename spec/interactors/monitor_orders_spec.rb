require 'rails_helper'

RSpec.describe MonitorOrders do
 describe '#call' do
  around { |e| VCR.use_cassette e.metadata[:full_description], &e }

  let(:market) { 'IOT-BTC' }
  let(:client) { ApiClient.new }
  let(:enquiry) do
    create :enquiry, sell_percent: 5, buy_percent: 5, market: market, sell_amount: 0.7
  end

  specify do
    bot = described_class.call(enquiry: enquiry)

    expect(bot).to be_success
    expect(client.my_orders(status: :open)['data'].count).to be >= enquiry.total_count

    expect(MarketPrice.first.sell_value).to be_present
    expect(MarketPrice.first.buy_value).to be_present
  end
 end
end
