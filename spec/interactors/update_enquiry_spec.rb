require 'rails_helper'

RSpec.describe UpdateEnquiry do
  describe '#call' do
    let!(:market_price) { create :market_price }
    let(:enquiry) { create :enquiry }

    context 'when buy related attributes' do
      specify do
        result = described_class.call(enquiry: enquiry, attributes: { buy_count: 10 })

        expect(market_price.reload.buy_value).to be_nil
        expect(market_price.sell_value).to be_present
      end
    end

    context 'when sell related attributes' do
      specify do
        result = described_class.call(enquiry: enquiry, attributes: { sell_count: 10 })

        expect(market_price.reload.buy_value).to be_present
        expect(market_price.sell_value).to be_nil
      end
    end

    context 'when buy and sell related attributes' do
      specify do
        result = described_class.call(enquiry: enquiry, attributes: { sell_count: 10, buy_count: 10 })

        expect(market_price.reload.buy_value).to be_nil
        expect(market_price.sell_value).to be_nil
      end
    end
  end
end
