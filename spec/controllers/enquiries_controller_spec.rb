require 'rails_helper'

RSpec.describe EnquiriesController, type: :controller do
  describe '#create' do
    context 'when not enough attributes' do
      specify do
        put :update, params: { enquiry: { market: '' } }

        expect(flash[:error]).to be_present
      end
    end

    context 'when not enough attributes' do
      let(:attributes) { attributes_for :enquiry }

      specify do
        put :update, params: { enquiry: attributes }

        expect(flash[:success]).to be_present
      end
    end
  end
end
