FactoryBot.define do
  factory :enquiry do
    market { Enquiry::MARKETS.sample }
    buy_count 1
    sell_count 1
    buy_percent 1
    sell_percent 1
    spend_amount 1
    sell_amount 1
  end
end
