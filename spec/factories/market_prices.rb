FactoryBot.define do
  factory :market_price do
    market { Enquiry::MARKETS.sample }
    buy_value "0.0025"
    sell_value "0.0025"
  end
end
