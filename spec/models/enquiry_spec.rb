require 'rails_helper'

RSpec.describe Enquiry, type: :model do
  describe 'validations' do
    subject { described_class.new(attrs) }

    context 'when market present' do
      let(:attrs) { { market: 'IOT-BTC' } }

      it { is_expected.to_not be_valid }

      context 'when partially filled attributes' do
        let(:attrs) do
          { market: 'IOT-BTC', buy_count: 1, sell_percent: 1, spend_amount: 1 }
        end

        it { is_expected.to_not be_valid }
      end

      context 'when buy_count and related attributes present' do
        let(:attrs) do
          { market: 'IOT-BTC', buy_count: 1, buy_percent: 1, spend_amount: 1 }
        end

        it { is_expected.to be_valid }

        context 'when market is nonexistent' do
          let(:attrs) do
            { market: 'BTC-GBP', buy_count: 1, buy_percent: 1, spend_amount: 1 }
          end

          it { is_expected.to_not be_valid }
        end
      end

      context 'when sell_count and related attributes present' do
        let(:attrs) do
          { market: 'IOT-BTC', sell_count: 1, sell_percent: 1, sell_amount: 1 }
        end

        it { is_expected.to be_valid }
      end
    end
  end
end
