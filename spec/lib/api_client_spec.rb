require 'rails_helper'

RSpec.describe ApiClient do
  around { |e| VCR.use_cassette e.metadata.dig(:example_group, :description), &e }

  let(:secret) { '758b382e-2912-45de-be6c-c8906a41f45a' }
  let(:key) { '13f68ce6-28d7-4257-9cf6-3a9559b34aa0' }
  let(:market) { 'IOT-BTC' }

  subject(:client) { described_class.new(key, secret) }

  describe '#orderbook' do
    specify do
      data = client.orderbook(market)["data"]

      expect(data['bids']).to be_any
      expect(data['asks']).to be_any
    end

    context 'with level' do
      let(:level) { 3 }

      specify do
        data = client.orderbook(market, level: level)["data"]

        expect(data['bids'].count).to be > 1
      end
    end
  end

  describe '#my_orders' do
    specify do
      data = client.my_orders["data"]

      expect(data).to be_any
    end

    context 'with filters' do
      specify do
        data = client.my_orders(market: market, status: :open)["data"]

        expect(data).to be_any
      end
    end
  end

  describe '#create_order' do
    let(:attrs) do
      {
        'market' => 'IOT-BTC',
        'operation_type' => 'limit_order',
        'order_type' => 'buy',
        'size' => '1.0',
        'price' => '0.000255'
      }
    end

    specify do
      data = client.create_order(attrs)["data"]

      expect(data["id"]).to be_a(Integer)
      expect(attrs.keys.map(&data)).to match_array attrs.values
    end
  end

  describe '#cancel_order' do
    let(:id) { client.my_orders(status: 'open')["data"].last["id"].to_s }

    specify do
      data = client.cancel_order(id)["data"]

      expect(data["status"]).to eq "canceled"
    end
  end
end
