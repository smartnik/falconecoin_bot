    RSpec.configure do
      VCR.configure do |config|
        config.cassette_library_dir = Rails.root.join('spec/fixtures/vcr_cassettes')
        config.hook_into :webmock
        config.configure_rspec_metadata!
        config.allow_http_connections_when_no_cassette = false
        config.ignore_localhost = true
      end
    end
