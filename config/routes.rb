Rails.application.routes.draw do
  root 'enquiries#show'
  resource :enquiries, only: %i(show update)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
