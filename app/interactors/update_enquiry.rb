class UpdateEnquiry
  include Interactor

  delegate :enquiry, :attributes, to: :context

  after do
    clear_market_price if market_price.present?
  end

  def call
    if enquiry.update(attributes)
      context.flash = { success: "Options updated! You'll see changes in a minute" }
    else
      context.flash = { error: enquiry.errors.messages.values.uniq.join(". \n") }
    end
  end

  private

  def market_price
    @market_price ||= MarketPrice.first
  end

  def clear_market_price
    market_price.update buy_value: nil if changed?('buy_count', 'buy_percent', 'spend_amount')
    market_price.update sell_value: nil if changed?('sell_count', 'sell_percent', 'sell_amount')
  end

  def changed?(*attributes)
    enquiry.previous_changes.values_at(*attributes).any?
  end
end
