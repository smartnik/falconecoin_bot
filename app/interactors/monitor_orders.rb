class MonitorOrders
  include Interactor

  TYPES = %i(sell buy)

  delegate :orders_count, :market, :spend_amount, :sell_amount, to: :enquiry
  delegate :enquiry, to: :context

  def call
    TYPES.each do |type|
      if run?(type)
        delete_orders(type)
        create_orders(type)
      end
    end
  end

  private

  def run?(type)
    previous_price = market_price.value_for(type: type)
    price = prices[type]

    previous_price.nil? || previous_price != price
  end

  def delete_orders(type)
    ids = client.my_orders(market: market)["data"].reduce([]) do |acc, order|
      suitable_to_cancel?(order, type) ? acc << order["id"] : acc
    end

    ids.each { |id| client.cancel_order id }
  end

  def create_orders(type)
    return if amount(type) <= 0

    save_price(type)

    orders_count[type].to_i.times do
      response = client.create_order(
        market: market,
        operation_type: :limit_order,
        order_type: type,
        size: amount(type).to_s,
        price: desired_prices[type].to_s
      )
    end
  end

  def desired_prices
    return @desired_prices if @desired_prices

    sell_percent = BigDecimal(enquiry.sell_percent)
    buy_percent = BigDecimal(enquiry.buy_percent)

    @desired_prices = {
      sell: BigDecimal(prices[:sell]) *  (1 + (sell_percent / 100.0)),
      buy: BigDecimal(prices[:buy]) *  (1 - (buy_percent / 100.0))
    }
  end

  def prices
    @prices ||= {
      buy: BigDecimal(orderbook["asks"].first["price"]),
      sell: BigDecimal(orderbook["bids"].first["price"])
    }
  end

  def amount(type)
    total[type] / orders_count[type]
  end

  def total
    @total ||= {
      buy: BigDecimal(spend_amount) / prices[:buy],
      sell: BigDecimal(sell_amount)
    }
  end

  def orderbook
    @bids ||= begin
      orderbook = client.orderbook(market)
      orderbook["data"]
    end
  end

  def client
    @client ||= ApiClient.new
  end

  def market_price
    @market_price ||= MarketPrice.find_or_create_by(market: market)
  end

  def suitable_to_cancel?(order, type)
    %w(open pending).include?(order["status"]) && order["order_type"] == type.to_s
  end

  def save_price(type)
    market_price.update "#{type}_value" => prices[type]
  end
end
