module EnquiriesHelper
  def markets
    Enquiry::MARKETS.map { |market| [market, market] }
  end

  def open_orders(options = {})
    @open_orders ||= ApiClient.new.my_orders(status: :open)['data'].group_by do |order|
      order['order_type']
    end
  end
end
