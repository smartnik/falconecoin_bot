class MonitorWorker
  include Sidekiq::Worker

  def perform
    enquiry = Enquiry.first
    return if enquiry.blank?
    MonitorOrders.call(enquiry: enquiry)
  end
end
