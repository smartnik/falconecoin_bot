class ApiClient
  attr_reader :key, :secret

  BASE_URL = 'https://staging.coinfalcon.com/api/v1/'

  def initialize(key = nil, secret = nil)
    @key = key || Rails.application.secrets.api_key
    @secret = secret || Rails.application.secrets.api_secret
  end

  def orderbook(market, options = {})
    url = BASE_URL + "markets/#{market}/orders"

    JSON.parse Typhoeus.get(url, params: options).body
  end

  def my_orders(options = nil)
    url = BASE_URL + "user/orders"
    headers = headers(url, method: 'GET', params: options)

    result = Typhoeus.get url, headers: headers, params: options
    JSON.parse result.body
  end

  def create_order(body)
    url = BASE_URL + "user/orders"
    headers = headers url, body: body, method: 'POST'

    result = request_with_retry do
      Typhoeus.post url, body: body, headers: headers
    end

    JSON.parse result.body
  end

  def cancel_order(id)
    url = BASE_URL + "user/orders"
    headers = headers url, method: 'DELETE', body: { id: id.to_s }

    result = request_with_retry do
      Typhoeus.delete url, headers: headers, body: { id: id }
    end

    JSON.parse result.body
  end

  private

  def headers(url, method:, body: nil, params: nil)
    timestamp = Time.now.to_i
    uri = URI(url)
    uri.query = URI.encode_www_form params if params

    signature = get_signature(timestamp: timestamp, path: uri.request_uri, body: body, method: method)
    {
      "CF-API-KEY" => key,
      "CF-API-TIMESTAMP" => timestamp,
      "CF-API-SIGNATURE" => signature
    }
  end

  def get_signature(timestamp:, path:, body: nil, method: 'GET')
    body = URI.unescape(body.to_json) if body.is_a?(Hash)
    payload = [timestamp, method, path, body].compact.join("|")

    OpenSSL::HMAC.hexdigest('sha256', secret, payload)
  end

  def request_with_retry(&block)
      result = block.call

      if result.code == 429
        Rails.logger.info "Too many requests. Sleeping"
        sleep(1.1)
        Rails.logger.info "Retrying"
        result = request_with_retry(&block)
      end

    result
  end
end
