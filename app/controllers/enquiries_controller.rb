class EnquiriesController < ApplicationController
  def show
    @enquiry = Enquiry.first || Enquiry.new
  end

  def update
    @enquiry = Enquiry.first || Enquiry.new

    result = UpdateEnquiry.call(enquiry: @enquiry, attributes: permitted_attributes)
    flash.replace(result.flash)

    render :show
  end

  private

  def permitted_attributes
    params.require(:enquiry).
      permit(:market, :buy_count, :sell_count, :buy_percent, :sell_percent, :spend_amount, :sell_amount)
  end
end
