class Enquiry < ApplicationRecord
  MARKETS = %w(IOT-BTC ETH-BTC LTC-BTC ETH-EUR BTC-EUR BCH-BTC ETH-BTC LTC-BTC ETH-EUR BTC-EUR BCH-BTC)

  validates :market, presence: true, inclusion: { in: MARKETS }

  validates :buy_count,
    presence: { message: 'Number of orders to sell should be specified' },
    unless: -> { sell_count.present? }

  validates :sell_count,
    presence: { message: 'Number of orders to buy should be specified' },
    unless: -> { buy_count.present? }

  validates :buy_percent, :spend_amount,
    presence: { message: 'Please specify buy percent and total amount to spend'},
    if: -> { buy_count.present? }

  validates :sell_percent, :sell_amount,
    presence: { message: 'Please specify sell percent and total amount of coins to sell'},
    if: -> { sell_count.present? }

  def orders_count
    { buy: buy_count, sell: sell_count }
  end

  def total_count
    buy_count + sell_count
  end
end
