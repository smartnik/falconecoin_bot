class MarketPrice < ApplicationRecord
  def value_for(type:)
    public_send "#{type}_value"
  end
end
