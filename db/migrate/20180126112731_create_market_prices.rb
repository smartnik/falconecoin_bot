class CreateMarketPrices < ActiveRecord::Migration[5.1]
  def change
    create_table :market_prices do |t|
      t.string :market
      t.string :sell_value
      t.string :buy_value

      t.timestamps
    end

    add_index :market_prices, %i(sell_value buy_value market)
  end
end
