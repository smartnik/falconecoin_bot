class CreateEnquiries < ActiveRecord::Migration[5.1]
  def change
    create_table :enquiries do |t|
      t.string :market
      t.integer :buy_count
      t.integer :sell_count
      t.string :buy_percent
      t.string :sell_percent
      t.string :spend_amount
      t.string :sell_amount

      t.timestamps
    end
  end
end
